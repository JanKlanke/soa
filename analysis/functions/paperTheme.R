#============================================================
# Function 
# (R-language Version)
# Author: Jan Klanke 
# Date: February 18th, 2019
#============================================================
#
#  Takes 3-Vector for text size, 1-No. for title justification
#  2-Vector for legend position and x- and y- achsis angles as
#  input and creates a set of themes 
#  
#------------------------------------------------------------

paperTheme <- function( xAchsis= 'there',
                       yAchsis= 'there',
                       xTangle= 'no',
                       yTangle= 'no',
                       LegPosi= 'none' ) {
  
  # Parameter ------------------------------------------------------------------------
  txtSize=  c( 10, 10, 10 )
  ttlJust=  0.5
  Font=     'Calibri' 
  xtxtAngl= ifelse( xTangle == 'no', 0, xTangle )
  xttlSize= 1
  ytxtAngl= ifelse( yTangle == 'no', 0, yTangle )
  yttlSize= 1
  hvJust=   if( xTangle == 'no' ) { c( 0.5, 0.5 ) } else { c( 1, 1 ) }
  tagPosi=  'topleft'
  grdColo=  '#FFFFFF'
  plotMrg=   margin(t= 0.25, r= 0.5, b= 0.25, l= 0, 'cm' )
  
  # Title -----------------------------------------------------------------------------
  Title <- theme( plot.title= element_blank() )
  
  # Axis ------------------------------------------------------------------------------
  # x-achsis
  if( xAchsis == 'there' ) {
    XAchs <- theme( axis.line.x= element_line(),
                    axis.ticks.x=element_line(),
                    axis.title.x= element_text( size= txtSize[2] * xttlSize, 
                                                family= Font ),
                    axis.text.x= element_text( size=txtSize[1], 
                                               hjust=hvJust[1], 
                                               vjust=hvJust[2], 
                                               angle= xtxtAngl, 
                                               family= Font ) )
  } else {
    XAchs <- theme(axis.line.x= element_blank(),
                   axis.ticks.x= element_blank(),
                   axis.title.x= element_blank(),
                   axis.text.x= element_blank() )
  }
  # y-achsis
  if( yAchsis == 'there' ) {
    YAchs<- theme(axis.line.y= element_line(),
                  axis.ticks.y= element_line(),
                  axis.title.y= element_text( size= txtSize[2] * yttlSize,
                                             angle= 90,
                                             family= Font),
                  axis.text.y= element_text( size=txtSize[1], 
                                             angle=ytxtAngl, 
                                             family=Font ) )
  } else {
    YAchs <- theme(axis.line.y= element_blank(),
                   axis.ticks.y= element_blank(),
                   axis.title.y= element_blank(),
                   axis.text.y= element_blank() )
  }
    
  # Leg, tag, and background ------------------------------------------------------------------
  # legend
  Legend  <- theme( legend.position= LegPosi )
  
  # tag     
  Tag  <-theme( plot.tag= element_text( size=txtSize[2], 
                                        family=Font ),
                plot.tag.position= tagPosi )
 
  # panel theme
  PnlTheme <- theme( panel.grid.major= element_line( colour= grdColo ),
                     panel.grid.minor= element_blank(),
                     panel.background= element_blank(), 
                     axis.line= element_line( colour= 'black' ),
                     plot.margin= plotMrg )
  
  # Export ------------------------------------------------------------------------------------
  
  grphTheme <- Title + XAchs + YAchs + Legend + Tag + PnlTheme
  
  return( grphTheme )
} 
