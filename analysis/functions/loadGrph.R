#============================================================
# Function 
# (R-language Version)
# Author: Jan Klanke 
# Date: February 18th, 2019
#============================================================
#
#  Takes 3-Vector for text size, 1-No. for title justification
#  2-Vector for legend position and x- and y- achsis angles as
#  input and creates a set of themes 
#  
#------------------------------------------------------------

loadGrph <- function(txtSize=c(10,12,14),ttlJust=0.5,Font='Helvetica',
                     xtxtAngl=0,xttlSize=1,
                     ytxtAngl=0,yttlSize=1,
                     hvJust=c(0.5,0.5),
                     legPosi='right',tagPosi='topleft',
                     grdColo=c('#FFFFFF'),
                     plotMrg=margin(0,0,0,0,"cm")) {
 
  # title
  TitleTher <- theme(plot.title=element_text(size=txtSize[3],hjust=ttlJust,family=Font))
  TitleGone <- theme(plot.title=element_blank())
  
  # axis 
  XAchsTher <- theme(axis.line.x=element_line(),axis.ticks.x=element_line(),
                     axis.title.x=element_text(size=txtSize[2]*xttlSize,family=Font),
                     axis.text.x=element_text(size=txtSize[1],hjust=hvJust[1],vjust=hvJust[2],angle=xtxtAngl,family=Font))
  XAchsTonL <- theme(axis.line.x=element_line(),axis.ticks.x=element_blank(),
                     axis.title.x=element_blank(),axis.text.x=element_blank())
  XAchsNonL <- theme(axis.line.x=element_blank(),axis.ticks.x=element_line(),
                     axis.title.x=element_text(size=txtSize[2]*xttlSize,family=Font),
                     axis.text.x=element_text(size=txtSize[1],hjust=hvJust[1],vjust=hvJust[2],angle=xtxtAngl,family=Font))
  XAchsGone <- theme(axis.line.x=element_blank(),axis.ticks.x=element_blank(),
                     axis.title.x=element_blank(),axis.text.x=element_blank())
  YAchsTher <- theme(axis.line.y=element_line(),axis.ticks.y=element_line(),
                     axis.title.y=element_text(size=txtSize[2]*yttlSize,angle=90,family=Font),
                     axis.text.y=element_text(size=txtSize[1],angle=ytxtAngl,family=Font))
  YAchsTonL <- theme(axis.line.x=element_line(),axis.ticks.x=element_blank(),
                     axis.title.x=element_blank(),axis.text.x=element_blank())
  YAchsNonL <- theme(axis.line.x=element_blank(),axis.ticks.x=element_line(),
                     axis.title.x=element_text(size=txtSize[2]*xttlSize,family=Font),
                     axis.text.x=element_text(size=txtSize[1],hjust=hvJust[1],vjust=hvJust[2],angle=xtxtAngl,family=Font))
  YAchsGone <- theme(axis.line.y=element_blank(),axis.ticks.y=element_blank(),
                     axis.title.y=element_blank(),axis.text.y=element_blank())
  # legend
  LegTher   <- theme(legend.title=element_blank(), legend.text=element_text(size=txtSize[1],family=Font),
                     legend.position=legPosi)
  LegGone   <- theme(legend.position='none')
  
  # tag     
  TagTher   <-theme(plot.tag=element_text(size=txtSize[2],family=Font),plot.tag.position=tagPosi)
  TagGone   <-theme(plot.tag=element_blank())
  
  # panel theme
  PnlThem   <- theme(panel.grid.major=element_line(colour=grdColo), panel.grid.minor=element_blank(),
                     panel.background=element_blank(), axis.line=element_line(colour='black'),
                     plot.margin=plotMrg)

  graphPars        <- list(TitleTher,TitleGone,XAchsTher,XAchsTonL,XAchsGone
                           ,YAchsTher,YAchsTonL,YAchsGone,LegTher,LegGone,TagTher,TagGone,PnlThem)
  names(graphPars) <- c('TitleTher','TitleGone','XAchsTher','XAchsTonL','XAchsGone',
                        'YAchsTher','XAchsTonL','YAchsGone','LegTher','LegGone','TagTher','TagGone','PnlThem')
  return(graphPars)
}
