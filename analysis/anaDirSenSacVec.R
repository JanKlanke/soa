# 
#============================================================
# Script "Influence of MS direction on perception (MS vector)"
# 
# Author: Jan Klanke 
# Date: February 2019
#============================================================
#  Calculate and plot:
#  (a) D' for all different combination of MS direction and 
#  direction of phase shift (MS left + PH left, MS left + 
#  PS right, MS right + PS left, and MS right + PS right).
#  (b) Distributions of the saccade vector of MS and their
#  directions (only horizontally, i.e. along the x-achsis).
#  (c) Saccade vectors of MS and their directions in 3-dim-
#  ensional space.
#  
#------------------------------------------------------------


#Set path appropriatly ----------------------------------------------------
setwd('/Users/Jan/')


# Libraries ---------------------------------------------------------------
libraries <- c('ggplot2','ggpubr','RColorBrewer','grid','gridExtra')
lapply(libraries, require, character.only=TRUE)


# Folder & form. spec. parameter ------------------------------------------
ffolder  <- './Dropbox/Analysis/functions/'            # subfolder with nec. functions
dfolder  <- './Desktop/dataSoA/preprocessed/'          # subfolder with the data
ofolder  <- './Dropbox/Analysis/figures/Sensitivity/dirSEN/sacVec/' # subfolder where to store output
iFSpec   <- '.csv'               # file format of input data
oFSpec   <- '.pdf'               # file format of output data


# Functions ---------------------------------------------------------------
sources <- c('loadData.R' ,'fltrPrtcp.R','fltrMS.R','fltrInOut.R',
             'fltrValRe.R','loadGrph.R')
lapply(sources,function(x) source(paste(ffolder,x,sep='')))


# Graph Pars -------------------------------------------------------------
dirTextSize <- c(7,10,12)
dirTagPos   <- 'right' # left coor. = horizontal shift || right coor.= vertical shift
dirLegPos   <- c(.9, .85)
dirHVju     <- c(0.5,1)
dirTitlJust <- 0.5  
dirLinCol   <- 'grey90'
dirColor    <- 'PRGn'

dirsenLb1   <- c('Microsaccade leftw.','Microsaccade leftw.','Microsaccade rightw.','Microsaccade rightw.')
dirsenLb2   <- c('Phase shift leftw.','Phase shift rightw.','Phase shift leftw.','Phase shift rightw.')

gPars <- loadGrph(txtSize=dirTextSize,ttlJust=dirTitlJust,hvJust=dirHVju,
                  tagPosi=dirTagPos,legPosi=dirLegPos,grdColo=dirLinCol)   


# Load data ---------------------------------------------------------------
data <- loadData(dfolder,iFSpec)  # load data  


# Load, filter, and transform data ----------------------------------------
newdata <- data

# set filter flags and filter data
insuffP <- list('PE' ,'PN')      # list of participants with bad data
insuffS <- list('all',1)         # list of participant sessions with bad data
insuffB <- list('all',c(1:6))    # list of blocks in those sessions with bad data
newdata <- fltrPrtcp(newdata,insuffP,insuffS,insuffB)  # filter bad participants
newdata <- fltrMS(newdata,'veryStrict',1)              # filter out all secondary (tertiary,...) MS and MS with amplitude>1
newdata <- fltrInOut(newdata)

# specify certain factors (/levels) to make data easier to work with
newdata$Cond <- c('non','rep','act')[newdata$expcon]          # ensures that conditions are recognizable
newdata$Resp[newdata$optOut==0] <- 1 # change response schema from 
newdata$Resp[newdata$optOut==1] <- 0 # 'have not seen' =1 to 'have seen'=1

# calculate saccade vector and MS direction in horizontal and vertical direction
newdata$sacVecX <- newdata$sacDist * cos(newdata$sacAngle1) 
newdata$sacVecY <- newdata$sacDist * sin(newdata$sacAngle1)
newdata$msDirX  <- sign(cos(newdata$sacAngle1))
newdata$msDirY  <- sign(sin(newdata$sacAngle1))

# label directions appropriately
newdata$stDirX[newdata$stiphad==  1] <- 'rgt'
newdata$stDirX[newdata$stiphad== -1] <- 'lft' 
newdata$msDirX[newdata$msDirX== 1] <- 'rgt'
newdata$msDirX[newdata$msDirX==-1] <- 'lft'

# Check for matches between movement direction of MS and phase shift
newdata$Match[newdata$msDirX==newdata$stiphad] <- 'Match'
newdata$Match[newdata$msDirX!=newdata$stiphad] <- 'No match'

# create zeros as the starting points for your velocity 'needles'
newdata$Strp <- rep(0,length(newdata$vpno))


# Aggregate data  ---------------------------------------------------------
# create the necessary subsets
newdataSubNon <- subset(newdata[newdata$Cond=='non',])
newdataSubAct <- subset(newdata[newdata$Cond=='act',])

# create necessecary function(s) 
calcPHit <- function(data,msdir,phdir) {
  lapply(split(data,data$vpno), function(x) {
    y = length(which(x$Resp==1 & x$msDirX==msdir & x$stDirX==phdir)) / length(which(x$msDirX==msdir & x$stDirX==phdir))
    if (length(which(x$msDirX==msdir & x$stDirX==phdir))==0) {y=NA}
    else {
      if (y==0){y = (1 / (2*length(which(x$msDirX==msdir & x$stDirX==phdir))))} 
      else if (y==1){y = 1 - (1 / (2*length(which(x$msDirX==msdir & x$stDirX==phdir))))}
    }
    x=y})
}

# calculate proportion of false alarms (dependent upon MS direction)
pFA_msl <- lapply(split(newdataSubNon,newdataSubNon$vpno), function(x) {
  y = length(which(x$Resp==1 & x$msDirX=='lft')) / length(which(x$msDirX=='lft'))
  if (y==0){y = (1 / (2*length(which(x$msDirX=='lft'))))} 
  else if (y==1){ y = 1-(1 / (2*length(which(x$msDirX=='lft'))))}
  x=y})
pFA_msr <- lapply(split(newdataSubNon,newdataSubNon$vpno), function(x) {
  y = length(which(x$Resp==1 & x$msDirX=='rgt')) / length(which(x$msDirX=='rgt'))
  if (y==0){y = (1 / (2*length(which(x$msDirX=='rgt'))))} 
  else if (y==1){ y = 1-(1 / (2*length(which(x$msDirX=='rgt'))))}
  x=y})

# calculate proportion of hits (depdent upon MS and phase shift direction)  
pHIT_msl_psl <- do.call('rbind',calcPHit(newdataSubAct,'lft','lft'))
pHIT_msl_psr <- do.call('rbind',calcPHit(newdataSubAct,'lft','rgt'))
pHIT_msr_psl <- do.call('rbind',calcPHit(newdataSubAct,'rgt','lft'))
pHIT_msr_psr <- do.call('rbind',calcPHit(newdataSubAct,'rgt','rgt'))

# bind together pFAs in data frame
pFA <- data.frame(msl=do.call('rbind',pFA_msl),
                  msr=do.call('rbind',pFA_msr))

# bind together pHITs in data frame
pHIT <- data.frame(msl_psl=pHIT_msl_psl,msl_psr=pHIT_msl_psr,
                   msr_psl=pHIT_msr_psl,msr_psr=pHIT_msr_psr)

# calculate and bind together dPrimes in data frame
dPri <- data.frame(msl_psl=qnorm(pHIT$msl_psl) - qnorm(pFA$msl),
                   msl_psr=qnorm(pHIT$msl_psr) - qnorm(pFA$msl),
                   msr_psl=qnorm(pHIT$msr_psl) - qnorm(pFA$msr),
                   msr_psr=qnorm(pHIT$msr_psr) - qnorm(pFA$msr))

# make it into a list (per participant)
dPrime <- lapply(split(dPri,seq(NROW(dPri))), function(x) {
    dPri=data.frame(DirMs=c('lft','lft','rgt','rgt'),
                    DirPh=c('lft','rgt','lft','rgt'),
                    dPrime=unname(t(x[1,])))})
names(dPrime) <- unique(newdata$vpno)


# Summary stats ----------------------------------------------------------
# calculate rowwise ttest for results (for the CIs)
tlist <- apply(dPri,2,t.test)

# again, extract relevant cols for CIs and bind them together
extract_tcols <- lapply(tlist,function(x) data.frame(mean=x$estimate, lwrBndry=x$conf.int[1], uprBndry=x$conf.int[2]))
ranges <- do.call('rbind',extract_tcols)

# transform data so that it is plot-ready
dPriSUM <- data.frame(DirMs=c('lft','lft','rgt','rgt'),
                      DirPh=c('lft','rgt','lft','rgt'),
                      dPrime=ranges[,1],
                      lwrBndry=ranges[,2],
                      uprBndry=ranges[,3])


# Plot stuff --------------------------------------------------------------
# plot of interaction of phase dir and ms dir for both eyes combined
plot1 <- lapply(dPrime, function(x)
  ggplot(data=x,aes(x=DirMs,y=dPrime,color=DirPh,fill=DirPh,group=DirPh),na.rm=TRUE) +
    geom_point() + geom_line() +
    scale_colour_manual(values = brewer.pal(11, dirColor)[c(2,10)]) +
    scale_fill_manual(values = brewer.pal(11, dirColor)[c(2,10)]) +
    # ylim(-1.5,2) + 
    ylab('Sensitivity d`') +
    ggtitle('Direction & saccade vector of MS (both eyes)'))
  
# density plot with the distribution of the velocities per ms along the horizontal dimension (x achsis) both eyes combined
plot2 <- lapply(split(newdata,newdata$vpno), function(x)
  ggplot(data=x,aes(x=sacVecX,fill=msDirX,color=msDirX,group=msDirX,na.rm=TRUE)) +
    geom_density() +
    scale_colour_manual(values = brewer.pal(11, dirColor)[c(2,10)]) +
    scale_fill_manual(values = brewer.pal(11, dirColor)[c(2,10)]) +
    geom_vline(data=as.data.frame(1),xintercept=0) +
    xlim(-2.5,2.5) + ylab('Density [a.u.]')) 
  
# plot with velocities and directions of ms for both eyes combined
plot3 <- lapply(split(newdata,newdata$vpno), function(x) 
  ggplot(data=x,na.rm=TRUE) + 
    geom_segment(aes(x=Strp,xend=sacVecX,y=Strp,yend=sacVecY,color=msDirX,group=msDirX), alpha=0.7,na.rm=TRUE) + 
    geom_point(aes(x=sacVecX,y=sacVecY,fill=msDirX,color=msDirX,group=msDirX), alpha=0.7,na.rm=TRUE) + 
    scale_colour_manual(values = brewer.pal(11, dirColor)[c(2,10)]) +
    geom_vline(data=as.data.frame(1),xintercept=0) +
    geom_hline(data=as.data.fram(1),yintercept=0) + 
    xlim(-2.5,2.5) + ylim(-2.5,2.5) + 
    xlab('hor. Distance [degree]') + ylab('ver. Distance [degree]'))


# Plot summary statistics -------------------------------------------------
# plot of interaction of phase directoion and MS dir for both eyes combined
plot1[[length(plot1)+1]] <- ggplot(data=dPriSUM,aes(x=DirMs,y=dPrime,ymin=lwrBndry,ymax=uprBndry,color=DirPh,fill=DirPh,group=DirPh),na.rm=TRUE) +
  geom_point() + geom_line() + geom_errorbar(aes(width=0.1)) +
  scale_colour_manual(values = brewer.pal(11, dirColor)[c(2,10)]) +
  scale_fill_manual(values = brewer.pal(11, dirColor)[c(2,10)]) +
  # ylim(-1.5,2) + 
  ylab('Sensitivity d`') +
  ggtitle('Direction & saccade vector of MS (both eyes)')

# density plot with the distribution of the MS vector lengthes per ms along the horizontal dimension
# (x achsis) for both eyes combined
plot2[[length(plot2)+1]] <- ggplot(data=newdata,aes(x=sacVecX,fill=msDirX,color=msDirX,group=msDirX,na.rm=TRUE)) +
  geom_density() +
  scale_colour_manual(values = brewer.pal(11, dirColor)[c(2,10)]) +
  scale_fill_manual(values = brewer.pal(11, dirColor)[c(2,10)]) +
  geom_vline(data=as.data.frame(1),xintercept=0) +
  xlim(-2.5,2.5) + ylab('Density [a.u.]')

# plot with MS vector and directions of ms for both eyes combined
plot3[[length(plot3)+1]] <- ggplot(data=newdata[!is.na(newdata$sacVecX),],na.rm=TRUE) + 
  geom_segment(aes(x=Strp,xend=sacVecX,y=Strp,yend=sacVecY,color=msDirX,group=msDirX), alpha=0.7,na.rm=TRUE) + 
  geom_point(aes(x=sacVecX,y=sacVecY,fill=msDirX,color=msDirX,group=msDirX), alpha=0.7,na.rm=TRUE) + 
  scale_colour_manual(values = brewer.pal(11, dirColor)[c(2,10)]) +
  geom_vline(data=as.data.frame(1),xintercept=0) +
  geom_hline(data=as.data.fram(1),yintercept=0) + 
  xlim(-2.5,2.5) + ylim(-2.5,2.5) + 
  xlab('hor. Distance [degree]') + ylab('ver. Distance [degree]')


# Adapt graph params ------------------------------------------------------
# apply changes to plots so that they look nice
plot1 <- lapply(plot1, function(x) 
  ggplotGrob(x + gPars$TitleTher + gPars$XAchsGone + gPars$YAchsTher + 
               gPars$TagTher + gPars$PnlThem + gPars$LegGone))
plot2 <- lapply(plot2, function(x) 
  ggplotGrob(x + gPars$TitleGone + gPars$XAchsTher + gPars$YAchsTher +
               gPars$TagGone + gPars$PnlThem + gPars$LegGone))
plot3 <- lapply(plot3, function(x) 
  ggplotGrob(x + gPars$TitleGone + gPars$XAchsNonL + gPars$YAchsNonL + 
               gPars$TagGone + gPars$PnlThem + gPars$LegTher))


# Saving ------------------------------------------------------------------
# final parameters for saving 
oName <- paste(ofolder,'dirSEN_sacVec_',c(paste(unique(newdata$vpno),sep=''),'ALL'),oFSpec,sep='')     # name for output file   

# save plots
for (i in 1:length(oName)) {
  finalPlot <- ggplotGrob(ggarrange(plot1[[i]],plot2[[i]],plot3[[i]],heights=c(0.25,0.5,1),widths=1,ncol=1,nrow=3,align='v'))
  pdf(file=paste(oName[i],sep=''),width=4.25)
  grid.newpage()
  grid.draw(finalPlot)
  dev.off()
}
