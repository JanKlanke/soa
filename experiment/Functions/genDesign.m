function design = genDesign(subjectCode)
%
% Notes on the design structure:
%
% General info:
%   .nTrain - number of training trials
%   .nTrial - number of test trial
%
% Trial info (.trial is replaced by .train in practice trials):
%   .trial(t).
%
% 2011 by Martin Rolfs || adapted 2018 by Jan Klanke

global visual scr keys const %#ok<NUSED>

% randomize random
rand('state',sum(100*clock));

% this subject's main sequence parameters
V0 = 450;A0 = 7.9;durPerDeg = 2.7;durInt = 23;

% participant parameters
design.suNo = uint8(subjectCode(9));
design.seNo = str2num(subjectCode(end));

% condition parameter
design.nTrialsPerCellInBlock = 4;               % number of trials per cell in block
design.condRat = [1 1 2 2 2 2 3 3 3 3];         % 1 = no stim, 2 = replay, 3 = active 
design.stiPres = [0 1 1];                       % 0 = no stim, 1 = stim pres    
design.appShif = [0 1 0];                       % 0 = no apperture shift, 1 = apperture shift
design.sacRequ = [0 0 1];                       % 0 = no saccade, 1 = saccade

% stimulus parameters
design.numStim = 1;         % number of stimuli
design.modDir = [1 -1];     % movement direction (leftward or rightward)

% settings for fixation (before cue)
design.timFixD = 0.000;                         % minimum fixation duration before stim onset [s]
design.timFixJ = 0.000;                         % additional fixation duration jitter before cue onset [s]

% timing settings
design.timAfKe = 0.200;                         % recording time after keypress  [s]
design.timMaSa = 1.000;                         % time to make a saccade (most likely)  [s]
design.iti     = 0.000;                         % inter-trial interval [s]

% onset settings for stimuli
design.timSti1 = 0.000;                         % delay of stimulus onset (after fixation duration end)  [s]
design.timStiD = 1*design.timMaSa;              % stimulus stream duration    [s]

% % stimulus velocities (non-zero are represented twice, as they need to be
% % run both top to bottom and bottom to top, contrary to zero-velocity.
% design.velIncr = [0];                          % velocity added to peak velocity
% design.velTtoB = [1 0];                        % increasing top to bottom (1) or botttom to top (0)

% stimulus parameters
design.numStim = 1;         % number of stimuli
design.modDir = [1 -1];     % movement direction (leftward or rightward)

for b = 1:1     % some arbitrary number of blocks for now
    t = 0;
    for cond = design.condRat
        for itri = 1:design.nTrialsPerCellInBlock
            t = t + 1;
            fprintf(1,'\n preparing trial %i ...',t);
            trial(t).trialNum = t; %#ok<*AGROW>

            % fixation positions
            fixx = -13 +(13+13)*rand;   % eccentricity of fixation x (relative to screen center) - can be pos or neg
            fixy =  -4 +( 4+ 4)*rand;   % eccentricity of fixation y (relative to screen center) - can be pos or neg
                        
            % saccade info
            % trial(t).sacReq = sare;   % 1 = saccade, 0 = cue but no saccade
            % determine v expected saccade velocities based on amplitudes
            % (using equation  and parameters used in Collewijn, 1988)
%                             trial(t).sacAmp = sqrt((design.fixPosX(fipo)-design.tarPosX(fipo))^2 + (design.fixPosY(fipo)-design.tarPosY(fipo))^2);
%                             trial(t).sacDir = sign(design.fixPosX(fipo));  
%                             trial(t).sacDir = sign(design.fixPosX(fipo));
            trial(t).sacReq = design.sacRequ(cond);   % 1 = saccade, 0 = cue but no saccade
            trial(t).sacAmp = 0.5;                                     
            trial(t).vpeaks = V0*(1-exp(-trial(t).sacAmp/A0));
            trial(t).sacDur = durPerDeg*trial(t).sacAmp + durInt;       % should be around 25 ms according to Martinez-Conde et al. 2004

            % temporal trial settings
            % duration before target onset [frames]
            trial(t).fixNFr = round((design.timFixD + design.timFixJ*rand)/scr.fd);
            trial(t).fixDur = trial(t).fixNFr*scr.fd;

            % duration after target onset [frames]
            trial(t).stiNFr = round((design.timSti1 + design.timStiD)/scr.fd);
            trial(t).stiDur = trial(t).stiNFr*scr.fd;

            % calculate total stimulus duration [frames]
            trial(t).totNFr = trial(t).fixNFr + trial(t).stiNFr;

            % timing parameter of stimulus presentation [frames]
            stiBeg = trial(t).fixNFr + 1 + round(design.timSti1/scr.fd);
            stiEnd = stiBeg + trial(t).stiNFr - 1;

            % timing parameters of apperture shift in replay condition [frames]
            trial(t).ramDur = round((design.timStiD/5)/scr.fd);             % frames until stimulus reaches full contrast                    
            shiBeg = stiBeg +  trial(t).ramDur; 
            shiEnd = stiEnd -  trial(t).ramDur;
            trial(t).shiDurNFr = round((trial(t).sacDur/1000)/scr.fd);      % in total, the movement takes place over the course of 54 frames - movement in first and last couple of fr. are so small, however, they are insignificant so that sac. duration is a good approximation
            trial(t).shiPeakTp = round((shiBeg+(trial(t).shiDurNFr/2)) + (rand * ((shiEnd-trial(t).shiDurNFr/2)-(shiBeg+trial(t).shiDurNFr/2))),0);          % frame number at which the shift of the Gaussian cornel reaches its peak (velocity)

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % generate flag streams for stimulus presentation %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % fixation
            fixBeg = 1;
            fixEnd = trial(t).fixNFr + trial(t).stiNFr;      
            trial(t).fixa.vis = zeros(1,trial(t).totNFr);
            trial(t).fixa.vis(fixBeg:fixEnd) = 1;
            trial(t).fixa.loc = visual.scrCenter+round(visual.ppd*[fixx fixy fixx fixy]);
            trial(t).fixa.col = visual.black;

            % determine stimulus parameters
            % spatial stimulus settings (standard)
            trial(t).stims.locX = trial(t).fixa.loc(1); 
            trial(t).stims.locY = trial(t).fixa.loc(2);

            % determine stimulus parameters
            % spatial stimulus settings (standard)
            trial(t).stims.col = repmat(visual.black,design.numStim,1);
            trial(t).stims.pars = getGaborPars(design.numStim);

            % define change in stimulus position across frames
            trial(t).posVec = zeros(design.numStim*2,trial(t).totNFr); 
            modi = randsample(design.modDir,1);             % motion direction of phase shift and Gaussian cornel         

            if design.appShif(cond)
                % for fixation trials, simulate the saccade
                % (will be replaced by actual saccade data in
                %  runTrials ... that's the plan at least)
                % org.: trial(t).posVec(1,stiBeg:stiEnd) = normcdf(stiBeg:stiEnd,mean([stiBeg stiEnd]),trial(t).sacDur/4);
                trial(t).posVec(1,shiBeg:shiEnd) = normcdf(shiBeg:shiEnd, trial(t).shiPeakTp, trial(t).sacDur/4);
                trial(t).posVec(1,shiEnd+1:stiEnd) = trial(t).posVec(1,shiEnd);
                
                % scale by actual saccade amplitude and
                % displace from initial fixation position
                % for the moment, this assumes horizontal
                % saccades. Also translates degrees to pixels.
                % org.: trial(t).posVec(1,:) = dva2pix(trial(t).sacDir*trial(t).sacAmp*trial(t).posVec(1,:) - design.fixPosX(fipo),scr); 
                trial(t).posVec(1,:) = dva2pix(modi*trial(t).sacAmp*trial(t).posVec(1,:),scr);  
            end

            % define modulation of top velocity across frames
            % (here, we are keeping stimulus velocity constant)
            velVec = ones(1,length(stiBeg:stiEnd));

%             % desired stimulus frequency [Hz]
%             if ttob
%                 % increasing speed from top to bottom
%                 trial(t).stims.tmpfrq = veli*(0: 1:(design.numStim-1))';
%             else
%                 % increasing speed from bottom to top
%                 trial(t).stims.tmpfrq = veli*((design.numStim-1):-1:0)';
%             end
            trial(t).stims.tmpfrq = 0; % should this be 60 Hz (s. Castet 2002)? 

            % changed from saccade dir. to (randomly chosen) movement 
            % dir because one can no longer infer the
            % saccade direction from  stimulus properties 
            % (as has been done originally)
            % org: trial(t).stims.evovel = trial(t).sacDir*(repmat(velVec*trial(t).vpeaks,design.numStim,1) + repmat(trial(t).stims.tmpfrq ./ trial(t).stims.pars.frq',1,length(velVec)));
            trial(t).stims.evovel = modi*(repmat(velVec*trial(t).vpeaks,design.numStim,1) + repmat(trial(t).stims.tmpfrq ./ trial(t).stims.pars.frq',1,length(velVec))); % desired stimulus velocity [dva per sec]

            % define phase change per frame for entire profile
            phaFra = scr.fd*trial(t).stims.evovel.*repmat(trial(t).stims.pars.frq'*360,1,length(velVec));  % phase change per frame [deg per fra]

            % define stimulus visibility and velocity
            trial(t).stims.vis = zeros(design.numStim,trial(t).totNFr);
            trial(t).stims.pha = zeros(design.numStim,trial(t).totNFr);
            if design.stiPres(cond)
                trial(t).stims.vis(:,stiBeg:stiEnd) = 1;
                trial(t).stims.pha(:,stiBeg:stiEnd) = cumsum(phaFra,2);
            end

            % add random phase to each stimulus
            trial(t).stims.pha = trial(t).stims.pha + repmat(360*rand(design.numStim,1),1,trial(t).totNFr);

            % define modulation of contrast across time
            ampVec = ones(1,length(stiBeg:stiEnd));
            ramVec = normcdf(1: trial(t).ramDur, trial(t).ramDur/2, trial(t).ramDur/6);
            ampVec(1: trial(t).ramDur) = ramVec;
            ampVec(end:-1:(end- trial(t).ramDur+1)) = ramVec;
            trial(t).stims.evoamp = repmat(trial(t).stims.pars.amp',1,length(ampVec)) .* repmat(ampVec,design.numStim,1);
            trial(t).stims.amp = zeros(design.numStim,trial(t).totNFr);
            trial(t).stims.amp(:,stiBeg:stiEnd) = trial(t).stims.evoamp;

            % define clock parameter
            trial(t).clock.vis = zeros(1,trial(t).totNFr);                                  % defines visibility of clock face & hand
            trial(t).clock.vis(fixBeg:stiEnd) = 1;                    % defines visibility of clock face & hand
            trial(t).clock.clockDurNFr = sum(trial(t).clock.vis);
            trial(t).clock.pars = getClockPars(trial(t).clock.clockDurNFr, trial(t).fixa.loc(1:2));   % creates all parameter for clock

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % define critical events during stimulus presentation %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            trial(t).events = zeros(1,trial(t).totNFr);
            trial(t).events(fixBeg) = 1;
            trial(t).events(stiBeg) = 2;
            trial(t).events(shiBeg) = 3;
            trial(t).events(shiEnd) = 4;
            trial(t).events(stiEnd) = 5;

            % time requirements for responses
            trial(t).maxSac = design.timMaSa;
            trial(t).aftKey = design.timAfKe;

            % store stimulus features
            trial(t).fixpox = fixx;                         % x coordinate of position of fixation [DVA rel. to midpoint]
            trial(t).fixpoy = fixy;                         % y coordinate of position of fixation [DVA rel. to midpoint] 
            trial(t).expcon = cond;                         % 1 = no stim  cond, 2 = replay cond, 3 = active cond
            trial(t).stipres = design.stiPres(cond);            % stimulus presence (1) vs. not present (0)
            trial(t).stiphad = design.stiPres(cond) * -modi;    % direction of phase shift [leftward (-1), rightward (1), no  phase shift (0)]
            trial(t).stimovd = design.stiPres(cond) * ~design.sacRequ(cond) * modi;     % direction of stimulus shift [leftward (-1), rightward (1), no  phase shift (0)]
            trial(t).stimovtpst = design.appShif(cond) * round(trial(t).shiPeakTp - trial(t).shiDurNFr/2) * (scr.fd*1000);       % time point of START of apperture shift [ms]
            trial(t).stimovtphv = design.appShif(cond) * trial(t).shiPeakTp * (scr.fd*1000);      % time point of HIGHEST VELOCITY of the stimulus [ms]
            trial(t).stimovdura = design.appShif(cond) * trial(t).shiDurNFr * (scr.fd*1000);      % duration of stimulus shift [ms]
            
            % store some clock features
            trial(t).cloOnPos = trial(t).clock.pars.cRand;          % onset position of clock hand [index of entry in position vector]
            trial(t).cSpeed = trial(t).clock.pars.Speed;            % clock speed as points on track
            trial(t).cloOnPosDgr = trial(t).clock.pars.cRandiDeg;   % onset position of clock hand [degree]
            trial(t).cSpeedpDgr = trial(t).clock.pars.speedpDeg;    % clock speed as duration per degree
            trial(t).vpeakd = modi*trial(t).vpeaks;  
        end
    end
    r = randperm(t);
    design.b(b).trial = trial(r);
end

design.nBlocks = b;                             % this number of blocks is shared by all qu
design.blockOrder = randperm(b);                % org: design.blockOrder = 1:b;

design.nTrialsPB = t;                           % number of trials per Block

save(sprintf('%s.mat',subjectCode),'design','visual','scr','keys','const');