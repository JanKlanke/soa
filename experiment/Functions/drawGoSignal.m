function drawGoSignal(col,loc)
%
% 2010 by Martin Rolfs

global scr visual

% org.: 
% pu = visual.ppd*0.1;
% Screen('FillOval',scr.myimg,col,loc+round([-pu -pu pu pu]));

pu = visual.ppd*0.1;
Screen('FrameOval',scr.myimg,col,loc+round(3*[-pu -pu pu pu]),pu);

