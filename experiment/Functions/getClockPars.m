function pars = getClockPars(clockDur, fixaLoc)
% 2014 by Martin Rolfs || adapted 2017 by Luke Pendergrass and 2018 by Jan
% Klanke

global visual scr %#ok<NUSED>

% Appearance Parameters
% for clockface
pars.numBands = 16;                         % number of bands
pars.ClockX = 5;                            % x size of elipse
pars.ClockY = 5;                            % y size of elipse
pars.bandCol1 = .70 * (visual.white);       % color, expressed as monitor luminance
pars.bandCol2 = .30 * (visual.white);       % color, expressed as monitor luminance
pars.Ccenter = fixaLoc;                     % clock center location
pars.CradiusD = 5;                          % DvA Clock radius              
pars.clockHandLengthD = 2;                  % clock hand length in DVA
pars.clockHandWidthD = .1;                  % clock hand length in DVA

% for speed
% pars.Speed = 256;                           clock speed as points on track  || org. =250 @ src.fd=0.0083 
pars.Speed = clockDur*2;                        % clock speed as points on track, trialDur*3*scr.refr means, that it'll move 1/3 of the clockface (or 120 d)
pars.cRand = round(pars.Speed*rand);      % clock onset position as vextor idx
if pars.cRand == 0
    pars.cRand = 1;
end
pars.cRandiDeg = (pars.cRand/pars.Speed)*360;         % clock onset position [degree]
pars.handPosIdxVec = [pars.cRand:pars.Speed 1:pars.cRand-1];
pars.speedpDeg = 360/(length(pars.handPosIdxVec)*(scr.fd*1000));    % clock speed time per degree [ms] 

% for report
pars.ReportOpt = pars.Speed;                % org.: 360
pars.randRPower = round(pars.ReportOpt*rand);
if pars.randRPower == 0
    pars.randRPower = 1;
end
pars.repHandPosIdxVec1 = [pars.randRPower:pars.ReportOpt 1:pars.randRPower-1];
% pars.repHandPosIdxVec2 = NaN(1,pars.Speed);
% pars.repHandPosIdxVec2(1:1440) = pars.repHandPosIdxVec1(1441:end);
% pars.repHandPosIdxVec2(1441:end) = pars.repHandPosIdxVec1(1:1440);

% DvA calculations
pars.Cradius = dva2pix(pars.CradiusD, scr);
pars.RectXsiz = dva2pix(pars.ClockX, scr);
pars.RectYsiz = dva2pix(pars.ClockY, scr);
pars.clockHandLength = dva2pix(pars.clockHandLengthD, scr);
pars.clockHandWidth  = dva2pix(pars.clockHandWidthD,scr);
pars.bandRect = [(pars.Ccenter(1)-round(pars.RectXsiz/2)) (pars.Ccenter(2)-round(pars.RectYsiz/2)) (pars.Ccenter(1)+round(pars.RectXsiz/2)) (pars.Ccenter(2)+round(pars.RectYsiz/2))];

% clock spatial parameter calculations
anglesDegr = linspace(0, 360, pars.Speed + 1);
anglesDegr(pars.Speed + 1) = [];
anglesRadr = anglesDegr * (pi / 180);
pars.VectorXunshifted = cos(anglesRadr) .* pars.Cradius + pars.Ccenter(1);
pars.VectorYunshifted = sin(anglesRadr) .* pars.Cradius + pars.Ccenter(2);
pars.VectorX = circshift(pars.VectorXunshifted, [(round(length(pars.VectorXunshifted)/4)),(round(length(pars.VectorYunshifted)/4))]);
pars.VectorY = circshift(pars.VectorYunshifted, [(round(length(pars.VectorYunshifted)/4)),(round(length(pars.VectorYunshifted)/4))]);
 
anglesDegr = linspace(0, 360, pars.Speed + 1);
anglesDegr(pars.Speed + 1) = [];
anglesRadr = anglesDegr * (pi / 180); 
pars.VectorRYunshifted = sin(anglesRadr) .* (pars.clockHandLength+pars.Cradius) + pars.Ccenter(2);
pars.VectorRXunshifted = cos(anglesRadr) .* (pars.clockHandLength+pars.Cradius) + pars.Ccenter(1);
pars.VectorRY = circshift(pars.VectorRYunshifted, [(round(length(pars.VectorRYunshifted)/4)),(round(length(pars.VectorRYunshifted)/4))]);
pars.VectorRX = circshift(pars.VectorRXunshifted, [(round(length(pars.VectorRXunshifted)/4)),(round(length(pars.VectorRYunshifted)/4))]);

% clockface graphical parameters
pars.facePos = [pars.VectorX pars.VectorX(1); pars.VectorY pars.VectorY(1)];   % 2xnumdot matrix of position of dot centers  
pars.bandLength = pars.Speed/pars.numBands;
pars.colVec1 = repmat(pars.bandCol1, 1, pars.bandLength);
pars.colVec2 = repmat(pars.bandCol2, 1, pars.bandLength);
pars.FaceColVec = repmat([pars.colVec1 pars.colVec2], 3, pars.numBands/2);
if pars.cRand > pars.Speed/2
    pars.faceCol = [pars.FaceColVec, pars.FaceColVec(:,1)];
else
    pars.faceCol = [fliplr(pars.FaceColVec), pars.FaceColVec(:,end)];
end

% clockhand graphical parameters
numdots = 4;
dotSizD = 0.8;
pars.handPos  = [pars.VectorX;  pars.VectorY];       % 2xnumdot matrix of position of dot centers  
pars.handPosR = [pars.VectorRX; pars.VectorRY];
pars.handSizD = dotSizD * [1 3/4 1/2 1/4];         % 1xnumdot vector for size of dots (reversed so that dots are drawn biggest to smallest) 
pars.handSiz = dva2pix(pars.handSizD, scr);
pars.handColD = repmat(repmat([0 1],1,(numdots/2)), 3, 1);% 3xnumdot matrix of colors of dots (colors are provided by rows)
pars.handColL = [0 0 0];

% % clockhand reporting options 
% anglesDegt = linspace(0, 360, pars.ReportOpt + 1);
% anglesDegt(pars.ReportOpt + 1) = [];
% anglesRadt = anglesDegt * (pi / 180); 
% pars.ReportHandPosVectorYunshifted = sin(anglesRadt) .* pars.Cradius + pars.Ccenter(2);
% pars.ReportHandPosVectorXunshifted = cos(anglesRadt) .* pars.Cradius + pars.Ccenter(1);
% pars.ReportHandPosVectorY = circshift(pars.ReportHandPosVectorYunshifted, [(round(length(pars.ReportHandPosVectorYunshifted)/4)), (round(length(pars.ReportHandPosVectorXunshifted)/4))]);
% pars.ReportHandPosVectorX = circshift(pars.ReportHandPosVectorXunshifted, [(round(length(pars.ReportHandPosVectorXunshifted)/4)), (round(length(pars.ReportHandPosVectorXunshifted)/4))]);
% pars.rephandPos = [pars.ReportHandPosVectorX; pars.ReportHandPosVectorY];

end