function [repPower,keypress] = getPowerMate(repPower, repOpt, keypress)
% 2018 by Jan Klanke || based on Luke Pendergrass 2017


if keypress ~= 0 && keypress ~= 1000
    repPower  = repPower + 10*keypress;
end

if repPower <= 0 || repPower >= repOpt
    if repPower <= 0
        repPower = repOpt + repPower;
    elseif repPower <= 0 && abs(ceil(repPower/repOpt)) ~= 0
        repPower = (ceil(repPower/repOpt) * repOpt) + repPower;
        if repPower == 0
            repPower = repOpt;
        end
    end
    if repPower >= repOpt
        repPower = 1;
    end
end

end