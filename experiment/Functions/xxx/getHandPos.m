function [pos] = getHandPos(rand, speed, fixNFr, nFr)

%2017 by Luke Pendergrass || adapted 2018 by Jan Klanke

global scr

% pos = rand + floor((nFr-fixNFr)/scr.rate);  % pos change is slowed down by scr.rate
pos = rand + (nFr-fixNFr);                % pos change is not slowed down  

if pos > speed && floor(pos/speed) == 0
    pos = pos - speed;
elseif pos > speed && floor(pos/speed) ~= 0
    pos = pos - (floor(pos/speed) * speed);
    if pos == 0
        pos = speed;
    end
end
end

