%test replaceMissedFrames

ols = 1:20; 
mfis = [1 2 12 20]; 
nls = replaceMissedFrames(ols,mfis); 

figure; hold on; 

for mi=mfis
    %plot([mi mi],[0 max([max(ols) max(nls)])],'k--');
    plot([0 length(nls)],ols([mi mi]),'k--');
end
for ri=(mfis-1)
    %plot([ri ri],[0 max([max(ols) max(nls)])],'y--');
    if ri==0
        plot([0 length(nls)],[0 0],'r--');
    else
        plot([0 length(nls)],ols([ri ri]),'r--');
    end
end


plot(1:length(ols),ols,'b.-'); 
plot(1:length(nls),nls,'r.-'); 
