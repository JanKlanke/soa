function Noise = getNoisePatch2(p)
%
% 2008 by Martin Rolfs

global visual

% First we compute pixels per cycle, rounded up to full pixels, as we
% need this to create a grating of proper size below:
ppc=ceil(1/p.frq);

[X,Y] = meshgrid(-p.siz:p.siz + ppc, -p.siz:p.siz);
Noise = visual.bgColor + visual.inColor * p.amp*2*(rand(size(X))-0.5);
