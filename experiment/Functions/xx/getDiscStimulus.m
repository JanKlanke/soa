function Disc = getDiscStimulus(p)
%
% Generates a circular checkerboard
%
% 2013 by Martin Rolfs

global visual


[x,y] = meshgrid(-p.siz:p.siz, -p.siz:p.siz);
Disc = visual.inColor * p.amp *((2*(x.^2 + y.^2 <= p.siz^2))-1);

