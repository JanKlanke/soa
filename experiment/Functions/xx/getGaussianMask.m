function Mask = getGaussianMask(p)
%
% 2013 by Martin Rolfs

global visual 

[X,Y] = meshgrid(-p.siz:p.siz,-p.siz:p.siz);

Mask = ones(2*p.siz+1, 2*p.siz+1, 2) * visual.bgColor;
Mask(:, :, 2) = visual.white * (1 - exp(-((X/p.sig).^2)-((Y/p.sig).^2)));

