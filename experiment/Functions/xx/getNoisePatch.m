function N = getNoisePatch(p)
%
% 2008 by Martin Rolfs

[X,Y] = meshgrid(-p.siz:p.siz,-p.siz:p.siz);
Noise    = p.amp*2*(rand(size(X))-0.5);
Grating  = p.amp*cos(X*(2*pi*p.frq*cos(p.ori)) + Y*(2*pi*p.frq*sin(p.ori))+p.pha) ;
Gaussian = exp(-(X.^2+Y.^2)./(2*p.sig^2)) ;
N = Gaussian.*Noise.*Grating;
