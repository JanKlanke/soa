function Grating = getCircularCheckerboard(p)
%
% Generates a circular checkerboard
%
% 2013 by Martin Rolfs

global visual

xylim = 2*pi*p.rcycles;

%AW: This original line makes the checkerboard be 1 pixel smaller than the circular mask:
%[x,y] = meshgrid(-xylim:2*xylim/(2*p.siz-1):xylim, -xylim:2*xylim/(2*p.siz-1):xylim);

%not subtracting 1 from 2*p.siz fixes it: 
[x,y] = meshgrid(-xylim:2*xylim/(2*p.siz):xylim, -xylim:2*xylim/(2*p.siz):xylim);

at = atan2(y,x);
Grating = visual.bgColor + visual.inColor * p.amp * ((sign(sin(at*p.tcycles)+eps) .* sign(sin(sqrt(x.^2+y.^2)))));

