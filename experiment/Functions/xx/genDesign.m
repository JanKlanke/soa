function design = genDesign(vpcode)
%
% Notes on the design structure:
%
% General info:
%   .nTrain - number of training trials
%   .nTrial - number of test trial
%
% Trial info (.trial is replaced by .train in practice trials):
%   .trial(t).
%
% 2011 by Martin Rolfs

global visual scr keys const %#ok<NUSED>

% randomize random
rand('state',sum(100*clock));

% this subject's main sequence parameters
V0 = 450;A0 = 7.9;durPerDeg = 2.7;durInt = 23;

% settings for fixation (before cue)
design.timFixD = 0.500;                         % minimum fixation duration before cue onset [s]
design.timFixJ = 0.500;                         % additional fixation duration jitter before cue onset [s]

% timing settings
design.timAfKe = 0.200;                         % recording time after keypress  [s]
design.timMaSa = 0.500;                         % recording time after keypress  [s]
design.iti     = 0.500;                         % inter-trial interval [s]

% onset settings for stimuli
design.timSti1 = 0.000;                         % stimulus onset re cue onset [s]
design.timStiD = 1*design.timMaSa;              % stimulus stream duration    [s]

% duration settings for stimuli

% saccade required?
design.nBlocks = 2;                             % this number of blocks is shared by all sacRequ
design.nTrialsPerCellInBlock = 1;
design.sacRequ = repmat([0 1],1, design.nBlocks/2);            % 1 = saccade, 0 = cue but no saccade

% stimulus velocities (non-zero are represented twice, as they need to be
% run both top to bottom and bottom to top, contrary to zero-velocity.
design.velStep = [0 2.5 2.5 5.0 5.0];           % velocity added to peak velocity
design.velTtoB = [1 0];                         % increasing top to bottom (1) or botttom to top (0)

design.offStim = [-8 -4 -2 0 +2 +4 +8;
                  -4 -2 -1 0 +1 +2 +4;
                   0  0  0 0  0  0  0;
                  +4 +2 +1 0 -1 -2 -4;
                  +8 +4 +2 0 -2 -4 -8];         % offset of first top stimulus

% stimulus positions
design.numStim = 5;
x = scr.resX/2 + linspace(  0, 0,design.numStim)*visual.ppd;
y = scr.resY/2 + linspace( -8, 8,design.numStim)*visual.ppd;
design.stiPosi = round([x;y;x;y]');  % stimulus positions (relative to screen center)

% other variables
design.fixPosX = [-5  5];                        % eccentricity of fixation x (relative to screen center)
design.fixPosY = [ 0  0];                        % eccentricity of fixation y (relative to screen center)
design.tarPosX = [ 5 -5];                        % eccentricity of target   x (relative to screen center)
design.tarPosY = [ 0  0];                        % eccentricity of target   y (relative to screen center)

b = 0;
for sare = design.sacRequ
    b = b + 1;
    t = 0;
    for itri = 1:design.nTrialsPerCellInBlock
        for offs = 1:size(design.offStim,1)
            for ttob = design.velTtoB
                for vels = design.velStep
                    for fipo = 1:length(design.fixPosX)
                        t = t + 1;
                        fprintf(1,'\n preparing trial %i ...',t);
                        trial(t).trialNum = t; %#ok<*AGROW>
                        
                        if sare
                            fixx  = design.fixPosX(fipo);
                            fixy  = design.fixPosY(fipo);
                            tarx  = design.tarPosX(fipo);
                            tary  = design.tarPosY(fipo);
                        else
                            fixx  = mean([design.fixPosX(fipo) design.tarPosX(fipo)]);
                            fixy  = mean([design.fixPosY(fipo) design.tarPosY(fipo)]);
                            tarx  = fixx;
                            tary  = fixy;
                        end
                        
                        % saccade info
                        trial(t).sacReq = sare;   % 1 = saccade, 0 = cue but no saccade
                        % determine v expected saccade velocities based on amplitudes
                        % (using equation  and parameters used in Collewijn, 1988)
                        trial(t).sacAmp = sqrt((design.fixPosX(fipo)-design.tarPosX(fipo))^2 + (design.fixPosY(fipo)-design.tarPosY(fipo))^2);
                        trial(t).sacDir = sign(design.fixPosX(fipo));
                        trial(t).vpeaks = V0*(1-exp(-trial(t).sacAmp/A0));
                        trial(t).sacDur = durPerDeg*trial(t).sacAmp + durInt;
                        
                        % temporal trial settings
                        % duration before target onset [frames]
                        trial(t).fixNFr = round((design.timFixD + design.timFixJ*rand)/scr.fd);
                        trial(t).fixDur = trial(t).fixNFr*scr.fd;
                        % duration after target onset [frames]
                        trial(t).stiNFr = round((design.timSti1 + design.timStiD)/scr.fd);
                        trial(t).stiDur = trial(t).stiNFr*scr.fd;
                        
                        % calculate total stimulus duration
                        trial(t).totNFr = trial(t).fixNFr + trial(t).stiNFr;
                        
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        % generate flag streams for stimulus presentation %
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        
                        % fixation
                        fixBeg = 1;
                        fixEnd = trial(t).fixNFr;
                        trial(t).fixa.vis = zeros(1,trial(t).totNFr);
                        trial(t).fixa.vis(fixBeg:fixEnd) = 1;
                        trial(t).fixa.loc = visual.scrCenter+round(visual.ppd*[fixx fixy fixx fixy]);
                        trial(t).fixa.col = visual.black;
                        
                        % target (for the saccade)
                        tarBeg = 1;
                        tarEnd = trial(t).fixNFr;
                        trial(t).targ.vis = zeros(1,trial(t).totNFr);
                        trial(t).targ.vis(tarBeg:tarEnd) = 1;
                        trial(t).targ.loc = visual.scrCenter+round(visual.ppd*[tarx tary tarx tary]);
                        trial(t).targ.col = visual.black;
                        
                        % stimulus 
                        stiBeg = trial(t).fixNFr + 1 + round(design.timSti1/scr.fd);
                        stiEnd = stiBeg + round(design.timStiD/scr.fd);

                        % determine stimulus parameters
                        for i = 1:design.numStim
                            % spatial stimulus settings (standard)
                            trial(t).stim(i).col = visual.black;
                            trial(t).stim(i).loc = design.stiPosi(i,:) + design.offStim(i,offs);
                            trial(t).stim(i).par = getGaborPars;
                            
                            % define modulation of top velocity across time
                            velVec = ones(1,length(stiBeg:stiEnd));
                            if ~sare
                                velSac = normpdf(stiBeg:stiEnd,mean([stiBeg stiEnd]),trial(t).sacDur/3);
                                velVec = velVec - velSac/normpdf(mean([stiBeg stiEnd]),mean([stiBeg stiEnd]),trial(t).sacDur/3);
                            end
                            
                            % desired stimulus frequency [Hz]
                            if ttob
                                % increasing speed from top to bottom
                                trial(t).stim(i).tmpfrq = vels * (i-1);
                            else
                                % increasing speed from bottom to top
                                trial(t).stim(i).tmpfrq = vels * (design.numStim-i);
                            end
                            trial(t).stim(i).evovel = trial(t).sacDir*(velVec*trial(t).vpeaks + trial(t).stim(i).tmpfrq / trial(t).stim(i).par.frq); % desired stimulus velocity [dva per sec]
                            
                            % define phase change per frame at top velocity
                            phaFra = scr.fd*trial(t).stim(i).evovel*trial(t).stim(i).par.frq*360;% phase change per frame [deg per fra]
                            
                            % define visibility and velocity
                            trial(t).stim(i).vis = zeros(1,trial(t).totNFr);
                            trial(t).stim(i).pha = zeros(1,trial(t).totNFr);
                            trial(t).stim(i).vis(stiBeg:stiEnd) = 1;
                            trial(t).stim(i).pha(stiBeg:stiEnd) = cumsum(phaFra);
                            
                            % define modulation of contrast across time
                            ampVec = ones(1,length(stiBeg:stiEnd));
                            ramDur = round((design.timStiD/10)/scr.fd);
                            ramVec = normcdf(1:ramDur,ramDur/2,ramDur/6);
                            ampVec(1:ramDur) = ramVec;
                            ampVec(end:-1:(end-ramDur+1)) = ramVec;
                            trial(t).stim(i).evoamp = trial(t).stim(i).par.amp * ampVec;
                            trial(t).stim(i).amp = zeros(1,trial(t).totNFr);
                            trial(t).stim(i).amp(stiBeg:stiEnd) = trial(t).stim(i).evoamp;
                        end
                        
                        % movement cue
                        movBeg = trial(t).fixNFr;
                        movEnd = trial(t).totNFr;
                        trial(t).move.vis = zeros(1,trial(t).totNFr);
                        % trial(t).move.vis(movBeg:movEnd) = 1; % commented to make disaappearance the go siganl
                        trial(t).move.loc = [trial(t).fixa.loc;trial(t).targ.loc];
                        trial(t).move.col = visual.white;
                        
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        % define critical events during stimulus presentation %
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        trial(t).events = zeros(1,trial(t).totNFr);
                        trial(t).events(fixBeg) = 1;
                        trial(t).events(movBeg) = 2;
                        trial(t).events(stiBeg) = 3;
                        trial(t).events(stiEnd) = 4;
                        
                        % time requirements for responses
                        trial(t).maxSac = design.timMaSa;
                        trial(t).aftKey = design.timAfKe;
                        
                        % store stimulus features
                        trial(t).fixpox = fixx;
                        trial(t).fixpoy = fixy;
                        trial(t).tarpox = tarx;
                        trial(t).tarpoy = tary;
                        trial(t).stiT2B = ttob;
                        trial(t).velStp = vels;
                        trial(t).fixpos = fipo;
                        trial(t).stiTil = sign(design.offStim(2,offs));
                        trial(t).stiOff = design.offStim(2,offs);
                    end
                end
            end
        end
    end
    r = randperm(t);
    design.b(b).trial = trial(r);
end

design.blockOrder = 1:b;

design.nTrialsPB = t;   % number of trials per Block

save(sprintf('%s.mat',vpcode),'design','visual','scr','keys','const');
