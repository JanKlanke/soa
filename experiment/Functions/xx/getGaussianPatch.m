function G = getGaussianPatch(p)
%
% 2008 by Martin Rolfs

[X,Y] = meshgrid(-p.siz:p.siz,-p.siz:p.siz);
Gaussian = exp(-(X.^2+Y.^2)./(2*p.sig^2)) ;
G = Gaussian.*p.amp ;
