function G = getGaborPatch(p)
%
% 2008 by Martin Rolfs

[X,Y] = meshgrid(-p.siz:p.siz,-p.siz:p.siz);
Grating  = p.amp*cos(X*(2*pi*p.frq*cos(p.ori)) + Y*(2*pi*p.frq*sin(p.ori))+p.pha) ;
Gaussian = exp(-(X.^2+Y.^2)./(2*p.sig^2)) ;
G = Gaussian.*Grating ;
