function Grating = getSinusoidalGrating2(p)
%
% Generates a simple sinusoidal grating (with certain orientation & phase)
%
% 2013 by Martin Rolfs

global visual

% First we compute pixels per cycle, rounded up to full pixels, as we
% need this to create a grating of proper size below:
ppc=ceil(1/p.frq);

[X,Y] = meshgrid(-p.siz:p.siz + ppc, -p.siz:p.siz);
Grating  = visual.bgColor + visual.inColor * p.amp * cos(X * 2*pi*p.frq + p.pha) ;
