function nL = genLumSeq(N)

global const

L = randn(N,1);              % define luminance signal
L_fft   = fft(L);           % Fourier transform
a_L_fft = abs(L_fft);       % Retain Magnitude (vector length in complex plane)
% normalize amplitudes of fourier components
nL_fft = L_fft./a_L_fft;
% inverse fourier transfrom to obtain normalized luminance signal
nL = ifft(nL_fft);

if const.lumType==2 %adjust variance  of the stimulus within the range 0 to 1
    % % VanRullen bring luminance signal into the range of 0 to 1, with a mean of
    % % 0.5 and a standard deviation of 0.5*1.7, which sacrifices the just
    % % obtained equal-power spectrum
    % nL = 0.5+0.5*(nL-mean(nL))./(std(nL)/1.7);
    
    %bring into a range of 0 to 1
    nL = (nL-min(nL))*1/(max(nL)-min(nL));
    
    %z-score, then expand to fit 95% of samples in the range 0 to 1 
    nL = 0.5+((nL-mean(nL))/std(nL))*0.5/1.96;

else %don't alter it
    % My plan was to span the whole range from 0 to 1
    nL = (nL-min(nL))*1/(max(nL)-min(nL));
end

%clip:
nL(nL<0)=0; nL(nL>1)=1;


%plot results 
% mags = abs(fft(nL));
% figure; subplot(1,2,1); hist(nL,100); subplot(1,2,2); plot(mags(2:end)); ylim([0 60]); 