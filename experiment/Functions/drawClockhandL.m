function drawClockhandL(pos, HposVecXY, HposVecRXY, HWidth, Hcol)
% 2017 by Luke Pendergrass || adapted 2018 by Jan Klanke

global scr
% pos = rand + idx-fixNFr;
% if idx-fixNFr > speed
%     pos = pos - speed;
% end
% if pos > speed
%     pos = pos - speed;
% end

Screen('DrawLine', scr.myimg, Hcol, HposVecXY(1,pos), HposVecXY(2,pos), HposVecRXY(1,pos), HposVecRXY(2,pos), HWidth);
end

