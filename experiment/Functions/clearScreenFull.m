function clearScreenFull 
% this function clears the screen and does this until the screen is refreshed 
% by RS 
 
global scr visual const 
 
if const.TEST == 0 % propixx is used 
    flipped = 0; 
    while flipped == 0 
        % org.: Screen('FillRect', scr.myimg, [scr.gray scr.gray scr.gray 0]); % clear screen 
        Screen('FillRect', scr.myimg, visual.bgColor); 
        flipped = PsychProPixx('QueueImage', scr.myimg); 
    end 
else % normal screen is used 
    Screen('Flip', scr.main); 
end 
