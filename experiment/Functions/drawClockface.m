function drawClockface(VecXY, colVec)
% org. by Luke Pendergrass 2017 ('FrameArcs') || adapted by Jan Klanke 2018

global scr

Screen('DrawDots', scr.myimg, VecXY, 3, colVec, [], 1); % draws clockface by means of 3 pixel dots

end