function [x,y] = getCoord

global const scr

if const.TEST
	[x,y,~] = GetMouse( scr.main );         % get gaze position from mouse							
else
	evt = Eyelink( 'newestfloatsample');	
	x   = evt.gx(const.DOMEYE)/2;			
	y   = evt.gy(const.DOMEYE)/2;			
end
