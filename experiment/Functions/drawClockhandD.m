function drawClockhandD(pos, posDot, sizDot, colDot, rand, speed, fixNFr, nFr)
% 2017 by Luke Pendergrass || adapted 2018 by Jan Klanke

global scr 

% draws clockhand dot
Screen('DrawDots', scr.myimg, repmat(posDot(1:2,pos),1,4), sizDot, colDot, [], 1);

end