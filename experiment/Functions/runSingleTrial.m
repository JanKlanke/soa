function [data,eyeData] = runSingleTrial(td)
%
% td = trial td
%
% 2016 by Martin Rolfs  || modified 2018 by Jan Klanke

global scr visual keys const 

% clear keyboard buffer
FlushEvents('KeyDown');
if ~const.TEST == 1
    HideCursor();
end

% Set the transparency for gabor patch
% Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);

% predefine boundary information
cxm = td.fixa.loc(1);
cym = td.fixa.loc(2);
% sxm = td.targ.loc(1);
% sym = td.targ.loc(2);
rad = visual.boundRad;
chk = visual.fixCkRad;

% draw trial information on operator screen
if ~const.TEST
%     Eyelink('command','draw_box %d %d %d %d 15', (sxm-rad)*2, (sym-rad)*2, (sxm+rad)*2, (sym+rad)*2);
    Eyelink('command','draw_box %d %d 15', (cxm-chk)*2, (cym-chk)*2);
end

% generate Procedural Gabor textures
nStim = length(td.stims.pars.sizp);
sti.tex = visual.procGaborTex;
sti.vis = td.stims.pars.sizp;
sti.src = [zeros(2,nStim); sti.vis; sti.vis].';
sti.dst = CenterRectOnPoint(sti.src, td.stims.locX,td.stims.locY);

for f = 1:td.totNFr
    stiFrames(f).dst = sti.dst.' + repmat(td.posVec(:,f),2,nStim);
end

% predefine time stamps
tFixaOn = NaN;  % t of fixation on
tStimOn = NaN;  % t of stimulus stream on
tStimCf = NaN;  % t of stimulus at full contrast
tStimCd = NaN;  % t of stimulus no longer at full contrast
tMoveOn = NaN;  % t of movement cue on
tStimOf = NaN;  % t of stimulus off
tSac    = NaN;  % t of saccade (if any)
tRes    = NaN;  % t of response (if any)
tClr    = NaN;  % t of of clear screen

% set flags before starting stimulus stream
eyePhase  = 1;  % 1 is fixation phase, 2 is saccade phase
breakIt   = 0;
fixBreak  = 0;
saccade   = 0;

% eyeData
eyeData = [];

% Initialize vector important for response
repPower = 1; % td.clock.pars.randRPower;
pmPress = 0;
kyPress = 0;
pressed = 0;
pringle = 0;

% Initialize vector to store data on timing
frameTimes   = NaN(1,td.totNFr);

% flip screen to start out time counter for stimulus frames
firstFlip = 0;nextFlip = 0;
while ~firstFlip
   firstFlip = PsychProPixx('QueueImage', scr.myimg);
end
% set frame count to 0
f = 0;
% get a first timestap
tLoopBeg = GetSecs;
t = tLoopBeg;
while ~breakIt && f < td.totNFr
    f = f+1;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % stimulus presentation %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
    for slo = 1:const.sloMo
        Screen('FillRect', scr.myimg, visual.bgColor);

        % stimuli
        % Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);
        if td.stims.vis(f)
            Screen('DrawTextures', scr.myimg, sti.tex, sti.src, stiFrames(f).dst, td.stims.pars.ori, [], [], [], [], kPsychDontDoRotation, [td.stims.pha(:,f), td.stims.pars.frqp', td.stims.pars.sigp', td.stims.amp(:,f), td.stims.pars.asp', zeros(nStim,3)]');
            drawCirc(visual.bgColor, td.fixa.loc);
        end
        % clockface
        if td.clock.vis(f)        
            drawClockface(td.clock.pars.facePos, td.clock.pars.faceCol);
            % drawClockhandD(td.clock.pars.handPosIdxVec(f), td.clock.pars.handPos, td.clock.pars.handSiz, td.clock.pars.handColD);
            drawClockhandL(td.clock.pars.handPosIdxVec(f),td.clock.pars.handPos, td.clock.pars.handPosR, td.clock.pars.clockHandWidth, td.clock.pars.handColL)
        end
        % fixation
        if td.fixa.vis(f)
            drawFixation(td.fixa.col,td.fixa.loc);
        end
        % Flip
        nextFlip = PsychProPixx('QueueImage', scr.myimg);
    end
    frameTimes(f) = GetSecs;
    
    %%%%%%%%%%%%%%%%%%%%%%%%
    % raise stimulus flags %
    %%%%%%%%%%%%%%%%%%%%%%%%

    % Send message that stimulus is now on
    if isnan(tFixaOn) && td.events(f)==1
        if ~const.TEST  ; Eyelink('message', 'EVENT_FixaOn'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_FixaOn'); end
        tFixaOn = frameTimes(f);
    end
%     if isnan(tMoveOn) && td.events(f)==2
%         if ~const.TEST  ; Eyelink('message', 'EVENT_MoveOn'); end
%         if  const.TEST>1; fprintf(1,'\nEVENT_MoveOn'); end
%         tMoveOn = frameTimes(f);
%         if td.sacReq == 1
%             eyePhase = 2;
%         end
%     end
    if isnan(tStimOn) && td.events(f)==2
        if ~const.TEST  ; Eyelink('message', 'EVENT_StimOn'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_StimOn'); end
        tStimOn = frameTimes(f);
    end
    if isnan(tStimCf) && td.events(f)==3
        if ~const.TEST  ; Eyelink('message', 'EVENT_StimCf'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_StimCf'); end
        tStimCf = frameTimes(f);
    end
    if isnan(tStimCd) && td.events(f)==4
        if ~const.TEST  ; Eyelink('message', 'EVENT_StimCd'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_StimCd'); end
        tStimCd = frameTimes(f);
    end
    if isnan(tStimOf) && td.events(f)==5
        if ~const.TEST  ; Eyelink('message', 'EVENT_StimOf'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_StimOf'); end
        tStimOf = frameTimes(f);
    end
    
    % eye position check
    if const.TEST<2
        [x,y] = getCoord;
        switch eyePhase
            case 1      % fixation phase
                if sqrt((x-cxm)^2+(y-cym)^2)>chk    % check fixation in a circular area
                    fixBreak = 1;
                end
%             case 2      % saccade phase
%                 % check if eyes are leaving fixation area
%                 if sqrt((x-cxm)^2+(y-cym)^2)>chk && isnan(tSac)
%                     tSac = GetSecs;
%                     if ~const.TEST, Eyelink('message', 'EVENT_Sac'); end
%                 end
%                 % check if eyes are reaching saccade target
%                 if sqrt((x-sxm)^2+(y-sym)^2)<rad && ~saccade
%                     saccade=1;
%                 end
        end
    end
    if fixBreak
        breakIt = 1;    % fixation break
%     elseif  td.sacReq==1 && ~saccade && t > tFixaOn + td.fixDur + td.maxSac
%         if const.TEST<2
%             breakIt = 2;    % no saccade in time
%         end
%     elseif  td.sacReq==1 &&  saccade &&  f == td.totNFr
%         breakIt = 3;    % saccade in time, stop when stimulus is done
% 	elseif  td.sacReq<=0 &&  f == td.totNFr
%         breakIt = 4;    % no saccade required, stop when stimulus is done
    end
    t = GetSecs;
end
lastFlip = 0;
while ~lastFlip && ~nextFlip
    Screen('FillRect', scr.myimg, visual.bgColor);
    lastFlip = PsychProPixx('QueueImage', scr.myimg);
end
tLoopEnd = GetSecs;

%%%%%%%%%%%%%%%%%%%%%%%%%
% stimulus presentation %
%%%%%%%%%%%%%%%%%%%%%%%%%
% Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
for i = 1:scr.rate
    Screen('FillRect', scr.myimg, visual.bgColor);
    PsychProPixx('QueueImage', scr.myimg);%,frameTimes(f-1) + 0.5*scr.fd*scr.rate);
end
% tStimOf = Screen('Flip',scr.main);%,frameTimes(f) + 0.5*scr.fd*scr.rate);
% if const.TEST<2, Eyelink('message', 'EVENT_StimOf'); end
% if const.TEST; fprintf(1,'\nEVENT_StimOf'); end
WaitSecs(td.aftKey/4);

switch breakIt
    case 1
        data = 'fixBreak';
        if const.TEST<2, Eyelink('command','draw_text 100 100 15 Fixation break'); end
%     case 2
%         data = 'tooSlow';
%         if const.TEST<2, Eyelink('command','draw_text 100 100 15 Too slow'); end
    otherwise
        % check for keypress
        % Snd('Play',[repmat(0.5,1,1050) linspace(0.5,0.0,50)].*[zeros(1,1000) sin(1:100)],5000);
        % nec. Parameters
        tResBeg = GetSecs;
        while ~pressed
            % get powermate
            [pmPress, tpPm] = checkTarPowerPress(keys.respButtons);
            [repPower,pmPress] = getPowerMate(repPower, td.clock.pars.ReportOpt, pmPress);
            % get keyboard 
            [kyPress, tpK] = checkTarPress(keys.resSpace);
            for i = 1:scr.rate
                % draw clockface
                Screen('FillRect', scr.myimg, visual.bgColor);
                % drawFixation(td.fixa.col,td.fixa.loc);
                drawClockface(td.clock.pars.facePos, td.clock.pars.faceCol);
                % draw reporthand
                % drawReportClockhandD(td.clock.pars.rephandPos, td.clock.pars.handSiz, td.clock.pars.handColD, td.clock.pars.repHandPosIdxVec1(repPower));
                drawClockhandL(td.clock.pars.repHandPosIdxVec1(repPower),td.clock.pars.handPos, td.clock.pars.handPosR, td.clock.pars.clockHandWidth, td.clock.pars.handColL);
                PsychProPixx('QueueImage', scr.myimg);
            end
            % determine wether powermate was used... 
            if pmPress == keys.enter
                % repIdx = repPower;
                repIdx = td.clock.pars.repHandPosIdxVec1(repPower);     % repPower
                tRes = tpPm;
                pringle = 1;
                pressed = 1;
            end
            % ...or the opt out option
            if kyPress
                repIdx = 10000;
                tRes = tpK;
                pringle = 2;
                pressed = 1;
            end
        end
        
        WaitSecs(td.aftKey);
        for f = 1:scr.rate
            Screen('FillRect', scr.myimg, visual.bgColor);
            PsychProPixx('QueueImage', scr.myimg);
        end
        
        tClr = GetSecs;
        if const.TEST<2, Eyelink('message', 'EVENT_Clr'); end
        if const.TEST; fprintf(1,'\nEVENT_Clr'); end
        
        %-------------------------%
        % PREPARE DATA FOR OUTPUT %
        %-------------------------%
        % collect trial information
        % org.:  trialData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%.2f\t%i\t%i\t%.2f',[td.fixpox td.fixpoy td.tarpox td.tarpoy td.fixpos td.stiTil td.stiOff td.stiT2B td.velInc td.totNFr td.sacReq td.vpeakd]);
        trialData = sprintf('%.2f\t%.2f\t%i\t%i\t%i\t%i\t%i\t%.2f\t%.2f\t%.2f\t%i\t%i\t%.2f\t%.2f\t%.2f',[td.fixpox td.fixpoy td.expcon td.stipres td.sacReq td.stiphad td.stimovd td.stimovtpst td.stimovtphv td.stimovdura td.cloOnPos td.cSpeed td.cloOnPosDgr td.cSpeedpDgr td.vpeakd]);
                      
        % org.: timeData  = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i',round(1000*([tFixaOn tMoveOn tStimOn tStimOf tSac tRes tClr]-tMoveOn)));
        timeData  = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i',round(1000*([tFixaOn tStimOn tStimCf tStimCd tStimOf tRes tClr]-tFixaOn)));
 
        % get response data
        % calculate information on behavioral response
        if pringle == 1
            optOut = 0;
            locMatch = find(repIdx == td.clock.pars.handPosIdxVec);     % -> Loc of clockhand at tp of stimVis  
            locMatchDeg = (repIdx/td.cSpeed)*360;
            clockTpJudge  = 1000*(locMatch*scr.fd);
            % nFrMatch = locMatch - (td.fixNFr);                          % Frame No at which the match occured is calculated: stim locked           
            % clockTpJudgeNFr = round(nFrMatch*scr.fd,4);                 % time point at which gabor was visible in [s]!
            if locMatch <= (td.fixNFr + td.ramDur)
                match = -1;
            elseif (locMatch > (td.fixNFr + td.ramDur)) && (locMatch < (td.totNFr - td.ramDur))
                match = 1;
            elseif locMatch >= (td.totNFr - td.ramDur)
                match = 0;
            end
            
        elseif pringle == 2     % resp. if participant opted out of the trial
            optOut = 1;         % (i.e. "did not see anything)  
            match = NaN;
            repIdx = NaN;       % 
            locMatch = NaN;
            locMatchDeg = NaN;
            clockTpJudge = NaN;
        end
        
        % determine presentation times [relative to 1st frame]        
        tResDur = (tRes - tResBeg)*1000;
        
        % org.: respData = sprintf('%i\t%i\t%i',round(1000*sacRT),round(1000*keyRT),resp);
        respData = sprintf('%i\t%i\t%i\t%.2f\t%.2f\t%.2f\t%.2f',[optOut match repIdx locMatch locMatchDeg clockTpJudge tResDur]);

        % get information about how timing of frames worked
        tLoopFrames = round((tLoopEnd-tLoopBeg)/scr.fd);
        frameData = sprintf('%i\t%i',tLoopFrames,td.totNFr);

        % collect data for tab [15 x trialData %i, 5 x timeData %i, 7 x respData %i, 2 x frameData %i]
        data = sprintf('%s\t%s\t%s\t%s',trialData, timeData, respData, frameData);
end
