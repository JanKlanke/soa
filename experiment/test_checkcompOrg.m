global scr visual

%% this subject's main sequence parameters
V0 = 450;A0 = 7.9;durPerDeg = 2.7;durInt = 23;

%% fixation & stim Positions

fixPosX = [2.5 -2.5  -5   0   5  2.5  -2.5];    
fixPosY = [  5    5   0   0   0   -5    -5];   

numStim = 4;
relStimDist = 100;
stimDistFix = 50;

veli = 1;
sare = [1 0];

for fipo = 1:length(fixPosX),
        fixx  = fixPosX(fipo);
        fixy  = fixPosY(fipo);

        stims.fixa_loc = visual.scrCenter+round(visual.ppd*[fixx fixy fixx fixy]);
        stims.locX = [stims.fixa_loc(1) + stimDistFix stims.fixa_loc(1) + 0 stims.fixa_loc(1) - stimDistFix stims.fixa_loc(1) + 0];
        stims.locY = [stims.fixa_loc(2) + 0 stims.fixa_loc(2) + stimDistFix stims.fixa_loc(2) + 0 stims.fixa_loc(2) - stimDistFix];

% 
%         if ~sare
%             design.numStim = 1; 
%             stiBeg = trial(t).fixNFr + 1 + round(design.timSti1/scr.fd);
%             stiEnd = stiBeg + trial(t).stiNFr - 1;
%             trial(t).stims.pars = getGaborPars(design.numStim);
%             randInd = randsample(4,1);
%             trial(t).stims.pars = getGaborPars(randInd);
%             trial(t).stims.locX = trial(t).stims.locX(randInd);
%             trial(t).stims.locY = trial(t).stims.locY(randInd);
%          end

        %% stimulus times
        timFixD = 1.500;
        timFixJ = 0.500;
        timMaSa = 0.500;                        
        timSti1 = 1.500;                         
        timStiD = 1*timMaSa;    

        fixNFr = round((timFixD + timFixJ*rand)/scr.fd);
        stiNFr = round((timSti1 + timStiD)/scr.fd);

        fixBeg = 1;
        fixEnd = fixNFr;
        stiBeg = fixNFr + 1 + round(timSti1/scr.fd);
        stiEnd = stiBeg + stiNFr - 1;


        %% stim Pos drawing
        totNFr = fixNFr + stiNFr;
        posVec = zeros(1,totNFr);
        sti.tex = visual.procGaborTex;
        stim.pars = getGaborPars(numStim);
        vis = stim.pars.sizp;
        src = [0 0 vis(1) vis(1)];

        for ii= 1:4,
            new(:,ii) = CenterRectOnPoint(src, stims.locX(ii), stims.locY(ii));
        end

        for f = 1:totNFr
            stiFrames(f).dst = new  + repmat(posVec(:,f),1,1);
        end

        %% phase shift
        sacAmp = 120/60;
        sacDir = sign(fixPosX(fipo));
        vpeaks = V0*(1-exp(-sacAmp/A0));
        sacDur = durPerDeg*sacAmp + durInt; 

        velVec = ones(1,length(stiBeg:stiEnd));
        tmpfrq = veli*(0: 1:(numStim-1))';
        evovel = sacDir*(repmat(velVec*vpeaks,numStim,1) + repmat(tmpfrq ./stim.pars.frq',1,length(velVec))); % desired stimulus velocity [dva per sec]
        phaFra = scr.fd*evovel.*repmat(stim.pars.frq'*360,1,length(velVec));% phase change per frame [deg per fra]
end 

