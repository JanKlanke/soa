close('all');
home;

addpath('/local/locallab/Documents/MATLAB/Toolboxes');
addpath('/local/locallab/Documents/MATLAB/Toolboxes/Palamedes');
addpath('/local/locallab/Documents/MATLAB/Toolboxes/PalamedesDemos');

%datFile = 'Data/MIPSRS01.dat';
%datFile = 'Data/MIPSTW01.dat';
%datFile = 'Data/MIPSMR01.dat';
%datFile = 'Data/MIPSSO01.dat';

d = load(datFile);

colStatic = [ 33  33  33]/255;  % gray
colTopSlo = [175 205  80]/255;  % green
colBotSlo = [162 113 174]/255;  % purple
styles = {'-', '-', '--'};

timBinCenter = -(30*12):12:(3*12);

frameOff = d(:,26)-d(:,25);

topOff = d(:, 9);
topSlo = d(:,10);
velInc = d(:,11);
vpeakd = d(:,14);
resTil = d(:,24);
sacDir = sign(vpeakd); % -1 = left-to-right

% mirror for saccade direction (now all rightward).
% ordering of speeds changes accordingly.
topSlo(sacDir==1) = ~topSlo(sacDir==1);

allOff = unique(topOff);
allVel = unique(velInc);

%Nelder-Mead search options
options = PAL_minimize('options');  %decrease tolerance (i.e., increase
options.TolX = 1e-09;              %precision).
options.TolFun = 1e-09;
options.MaxIter = 10000;
options.MaxFunEvals = 10000;

% general fitting settings
searchGrid.alpha  = min(allOff):.05:max(allOff);       % structure defining grid to
searchGrid.beta   = 10.^(-1:.05:2); % search for initial values
searchGrid.gamma  = 0:.005:.1;      % type help PAL_PFML_Fit for more information
searchGrid.lambda = 0:.005:.1;
PF = @PAL_Logistic;                 % PF function

paramsFree = [1 1 1 1]; %[threshold slope guess lapse] 1: free, 0:fixed

figure;
hold('on');

li = 0;   % legend index
% top slow
for v = 2:length(allVel)
    for o = 1:length(allOff)
        idx = topOff==allOff(o) & velInc == allVel(v) & topSlo == 1;
        nCW(o) = sum(resTil(idx)==1);
        ntr(o) = sum(idx);
        pCW_t2b(v-1,o) = nCW(o)/ntr(o);
    end
    % fit psychometric function
    paramsFitted = PAL_PFML_Fit(allOff', nCW, ntr, searchGrid, paramsFree, PF,...
        'lapseLimits',[0 0.5],'guessLimits',[0 0.5],'searchOptions',options);
    
    li = li + 1;
    hPlot(li) = plot(searchGrid.alpha,PF(paramsFitted,searchGrid.alpha),char(styles{v}),'color',colTopSlo,'linewidth',2);
    sLegend{li} = sprintf('Top slow %.1f Hz',allVel(v));
    if v>2
        filCol = [1 1 1];
    else
        filCol = colTopSlo;
    end
    plot(allOff,pCW_t2b(v-1,:),'o','color',colTopSlo,'MarkerFaceColor',filCol,'LineWidth',2);
end

% bottom slow
for v = 2:length(allVel)
    for o = 1:length(allOff)
        idx = topOff==allOff(o) & velInc == allVel(v) & topSlo == 0;
        nCW(o) = sum(resTil(idx)==1);
        ntr(o) = sum(idx);
        pCW_b2t(v-1,o) = nCW(o)/ntr(o);
    end
    % fit psychometric function
    paramsFitted = PAL_PFML_Fit(allOff', nCW, ntr, searchGrid, paramsFree, PF,...
        'lapseLimits',[0 0.5],'guessLimits',[0 0.5],'searchOptions',options);
    
    li = li + 1;
    hPlot(li) = plot(searchGrid.alpha,PF(paramsFitted,searchGrid.alpha),char(styles{v}),'color',colBotSlo,'linewidth',2);
    sLegend{li} = sprintf('Top fast %.1f Hz',allVel(v));
    if v>2
        filCol = [1 1 1];
    else
        filCol = colBotSlo;
    end
    plot(allOff,pCW_b2t(v-1,:),'o','color',colBotSlo,'MarkerFaceColor',filCol,'LineWidth',2);
end

% static
for o = 1:length(allOff)
    idx = topOff==allOff(o) & velInc == 0;
    nCW(o) = sum(resTil(idx)==1);
    ntr(o) = sum(idx);
    pCW_sta(o) = nCW(o)/ntr(o);
end
% fit psychometric function
paramsFitted = PAL_PFML_Fit(allOff', nCW, ntr, searchGrid, paramsFree, PF,...
    'lapseLimits',[0 0.5],'guessLimits',[0 0.5],'searchOptions',options);

li = li + 1;
hPlot(li) = plot(searchGrid.alpha,PF(paramsFitted,searchGrid.alpha),char(styles{1}),'color',colStatic,'linewidth',2);
sLegend{li} = sprintf('Static %.1f Hz',0);
plot(allOff,pCW_sta,'o','color',colStatic,'MarkerFaceColor',colStatic,'LineWidth',2);

title('Normalized for rightward motion')
xlabel('Magnitude of cw slant');
xlim([-0.5 0.5]+[min(allOff) max(allOff)]);
ylabel('Proportion of cw reports');
ylim([0 1]);
if li == 3
    l = legend(hPlot([1 3 2]),sLegend{[1 3 2]});
elseif li == 5
    l = legend(hPlot([2 1 5 3 4]),sLegend{[2 1 5 3 4]});
end
set(l,'Box','off','FontSize',12,'Location','SouthEast');
figure;

hold('on');

h = hist(frameOff,timBinCenter);

idxCor = timBinCenter>=-6 & timBinCenter<12;
bar(timBinCenter(idxCor),h(idxCor),'g','barwidth',12)
idxNeg = timBinCenter<-6;
bar(timBinCenter(idxNeg),h(idxNeg),'r','barwidth',1)
idxPos = timBinCenter>12;
bar(timBinCenter(idxPos),h(idxPos),'y','barwidth',1)

fprintf(1,'\nTrial had too many  frames: %2.1f',100*sum(frameOff<-6)/length(frameOff));
fprintf(1,'\nTrial had too few   frames: %2.1f',100*sum(frameOff>=12)/length(frameOff));
fprintf(1,'\nTrial had correct # frames: %2.1f',100*sum(frameOff>=-6 & frameOff<12)/length(frameOff));
