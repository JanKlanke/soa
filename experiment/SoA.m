%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Role of volition and awareness in SoA %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 2016 by Martin Rolfs || adapted 2018 by Jan Klanke
%
% Observers have to fixate central fixation dot. After fixation period, a 
% Gabor grating will be presented that features a phase shift of high
% frequency (>= predicted peak velocity of a microsaccade) so as to be 
% invisible under normal viewing conditions. Only if participant engages in 
% microsaccade will the stimulus be visible. Participants have to report
% the time at which they perceived the Gabor appear. Report is done by 
% adjusting the hand of a clock to the position in which it was when the 
% stimulus appeared.
%
% TO DO:
% 
% 
% 
% 
% 
% 

clear all;
clear mex;
clear functions; 

addpath('Functions/','Data/');

home;
expStart=tic;

global const visual scr keys df knob %#ok<*NUSED>

const.TEST = 2;         % test in dummy mode? 0=with eyelink; 1=mouse as eye; 2=no gaze position checking of any sort
const.sloMo= 48*2;         % To play stimulus in slow motion, draw every frame const.sloMo times

exptname='SoA';

try
    newFile = 0;
    while ~newFile
        subjectCode = getSubjectCode(exptname);
        subjectCode = strcat('Data/',subjectCode);
        
        % create data file
        datFile = sprintf('%s.dat',subjectCode);
        if exist(datFile,'file')
            o = input('>>>> This file exists already. Should I overwrite it [y / n]? ','s');
            if strcmp(o,'y')
                newFile = 1;
            end
        else
            newFile = 1;
        end
    end
    
    % prepare screens
    prepScreen;
    
    % initialize Powermate;
    InitializePowermate;
    
    % disable keyboard
    ListenChar(2);
    
    % prepare stimuli
    prepStim;
    
    % generate design
    design = genDesign(subjectCode);
    
    % initialize eyelink-connection
    if const.TEST<2
        [el, err]=initEyelinkNew(subjectCode(6:end));
        if err==el.TERMINATE_KEY
            return
        end
    else
        el=[];
    end
    
    % runtrials
    design = runTrials(design,datFile,el);
    
    % shut down everything
    reddUp;
catch me
    rethrow(me);
    reddUp; %#ok<UNRCH>
end

expDur=toc(expStart);

fprintf(1,'\n\nThis (part of the) experiment took %.0f min.',(expDur)/60);
fprintf(1,'\n\nOK!\n');

plotReplayResults(datFile);