function [p] = plotReplayResults(subFileName)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

global scr
% 
% subFileName = 'Data/SoAjp71.dat';
% 
% 
% data= [data1; data2];

% load data
data = load(subFileName);

% define necessary vars
actShiTpSt = [];
actShiTpHV = [];
repShiTp = [];

for i = 1:length(data(:,1))
    if data(i,12) ~=0
        actShiTpSt  = [actShiTpSt data(i,12)];       % actual time of shift onset re fixation onset [ms]
        actShiTpHV  = [actShiTpHV data(i,13)];       % actual time of shift onset re fixation onset [ms] 
        repShiTp    = [repShiTp data(i,32)];         % reported time of shift (relative to random onset position of the clock hand [ms]
    end
end

% calculate outcomes
IntervalsSt = repShiTp-actShiTpSt;
IntervalsHV = repShiTp-actShiTpHV;

% draw plot
p= figure;
str = sprintf('Participant %s - Results in the replay condition', subFileName(4:7));
suptitle(str);
hold on
subplot(2,2,1);
hold on
nbins = length(data(:,1))/10;
histogram(actShiTpSt, nbins)
histogram(repShiTp, nbins)
title('Tp_a_c_t_S_t vs Tp_r_e_s_p')
xlabel('Tp re trial onset [ms]');
ylabel('Frequency count');
legend('Tp_a_c_t_S_t', 'Tp_r_e_s_p');
hold off

subplot(2,2,2);
hold on
histogram(IntervalsSt, nbins)
title('Interval_D_u_r: Tp_r_e_s_p - Tp_a_c_t_H_V')
xlabel('Interval length [ms]');
ylabel('Frequency count');
legend('RepInterval [ms]');
hold off

subplot(2,2,3);
hold on
histogram(actShiTpHV, nbins)
histogram(repShiTp, nbins)
title('Tp_a_c_t_H_V vs Tp_r_e_s_p')
xlabel('Tp re trial onset [ms]');
ylabel('Frequency count');
legend('Tp_a_c_t_H_V', 'Tp_r_e_s_p');
hold off

subplot(2,2,4);
hold on
histogram(IntervalsHV, nbins)
title('Interval_D_u_r: Tp_r_e_s_p - Tp_a_c_t_H_V')
xlabel('Interval length [ms]');
ylabel('Frequency count');
legend('RepInterval [ms]');
hold off
end
