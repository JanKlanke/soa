#============================================================
# Microsaccade Toolbox 0.9
# Demo file
# (R-language Version)
# Authors: Ralf Engbert, Petra Sinn, Konstantin Mergenthaler, 
# and Hans Trukenbrod
# Date: February 20th, 2014
#============================================================
rm(list=ls())
setwd("~/Desktop/Meth-HU-Berlin/MS_Toolbox_R")
source("vecvel.R")
source("smoothdata.R")
source("microsacc.R")
source("binsacc.R")
source("surrogate.R")
par(ask = TRUE)

#------------------------------
# 1. Detection of microsaccades 
#------------------------------
# Set parameters
SAMPLING = 500
MINDUR = 3
VFAC = 5

# Read raw data (ASCII; subject 01, trial 005)
d <- read.table("data/f01.005.dat")

# Select epoch from trial, transform to matrix
idx <- 3001:4500
xl <- as.matrix(d[idx,2:3])
xr <- as.matrix(d[idx,4:5])

# Apply running average
xls <- smoothdata(xl)
xrs <- smoothdata(xr)

# Detection of microsaccades
msl <- microsacc(xl,VFAC,MINDUR,SAMPLING)
msr <- microsacc(xr,VFAC,MINDUR,SAMPLING)
sac <- binsacc(msl$table,msr$table)

# (Binocular) microsaccades
N <- sac$N[1]
bin <- sac$bin

# Plot trajectory
par(mfrow=c(1,2))
plot(xls[,1],xls[,2],type='l',asp=1,
     xlab=expression(x[l]),ylab=expression(y[l]),
     main="Position")
for ( s in 1:N ) {
  j <- bin[s,1]:bin[s,2] 
  lines(xls[j,1],xls[j,2],type='l',col='red',lwd=3)
}
points(xls[bin[,2],1],xls[bin[,2],2],col='red')

# Plot trajectory in 2D velocity space
vls <- vecvel(xl,SAMPLING)
plot(vls[,1],vls[,2],type='l',asp=1,
     xlab=expression(v[x]),ylab=expression(v[y]),
     main="Velocity")
for ( s in 1:N ) {
  j <- bin[s,1]:bin[s,2] 
  lines(vls[j,1],vls[j,2],type='l',col='red',lwd=3)
}
phi <- seq(from=0,to=2*pi,length.out=300)
cx <- msl$radius[1]*cos(phi)
cy <- msl$radius[2]*sin(phi)
lines(cx,cy,lty=2)


#------------------------------------
# 2. Generation of surrogate data
#------------------------------------
# Generation of surrogates
xls <- smoothdata(xl)
xlsur <- surrogate(xls)

# Plot original and surrogate trajectories
par(mfrow=c(1,2))
plot(xls[,1],xls[,2],type='l',main="Original data",xlab="x",ylab="y")
plot(xlsur[,1],xlsur[,2],type='l',col='red',main="AAFT surrogates",xlab="x",ylab="y")

# Plot acf of original trajectory and surrogate tracetory
vls <- vecvel(xls)
a1 <- acf(vls[,1],plot=FALSE)
par(mfrow=c(1,1))
plot(a1,type='l',main="Autocorrelation function")
for ( s in 1:20 )  {
  xlsur <- surrogate(xls)
  vlsur <- vecvel(xlsur,TYPE=2)
  a2 <- acf(vlsur[,1],plot=FALSE)
  lines(a2$lag,a2$acf,type='l',col='red')
}
lines(a1$lag,a1$acf,type='l',col='black')


#-------------------------------------------------
# 3. Surrogate analysis for different thresholds
#-------------------------------------------------
# range of detection thresholds
vfac = seq(from=3,to=8,by=0.5)

# subjects and trials
vp <- 1
ntrials <- 5

# read raw data
num = length(vfac)
mstab <- matrix(rep(0,3*num),ncol=3)
for ( v in 1:vp ) {
  for ( f in 1:ntrials ) {
    cat("... processing: vp =",v,", trial =",f,"\n")
    filename <- paste("data/f0",v,".00",f,".dat",sep="")
    d <- read.table(filename)
    # select epoch, transform to matrix
    xl <- as.matrix(d[,2:3])
    xr <- as.matrix(d[,4:5])
    dur <- dim(xr)[1]/SAMPLING
    for ( i in 1:num) {
      # detect microsaccades
      msl <- microsacc(xl,VFAC=vfac[i])
      msr <- microsacc(xr,VFAC=vfac[i])
      sac <- binsacc(msl$table,msr$table)
      N <- sac$N[1]/dur
      # computation of surrogate data
      xsl <- surrogate(xl)
      xsr <- surrogate(xr)
      msl <- microsacc(xsl,VFAC=vfac[i])
      msr <- microsacc(xsr,VFAC=vfac[i])
      sac <- binsacc(msl$table,msr$table)
      Nsur <- sac$N[1]/dur
      mstab[i,1] <- vfac[i]
      mstab[i,2:3] <- mstab[i,2:3] + c(N,Nsur)
    }
  }
}
mstab[,2:3] <- mstab[,2:3]/(vp*ntrials)

# Plot surrogate analysis
par(mfrow=c(1,1))
plot(mstab[,1],mstab[,2],type='b',ylim=c(0,3),
     xlab=expression("Threshold multiplier  "*lambda),ylab="Rate [1/s]",
     main="Surrogate analysis")
lines(mstab[,1],mstab[,3],type='b',col='red')
lines(mstab[,1],mstab[,2]-mstab[,3],type='b',col='blue')
legend(6,3,c('original data','surrogates','difference'),
       lty=c(1,1,1),pch=c(1,1,1),col=c('black','red','blue'))

